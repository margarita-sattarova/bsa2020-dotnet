﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA20HW1
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        public List<Task> Tasks { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
        public Team Team { get; set; }
        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
    }
}
