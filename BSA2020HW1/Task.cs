﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA20HW1
{
    public class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        public User Performer { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        [JsonProperty("name")]

        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public int State { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
    }
}
