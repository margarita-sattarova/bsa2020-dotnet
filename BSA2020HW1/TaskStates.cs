﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA20HW1
{
    public enum TaskStates
    {
        Created,
        Started, 
        Finished,
        Canceled
    }
}
