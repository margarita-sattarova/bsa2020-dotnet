﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using Task = BSA20HW1.Task;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BSA20HW1
{
    class MenuProvider
    {
        static HttpClient client = new HttpClient();
        public static bool toContinue { get; private set; } = true;
        private static Action[] MenuActions;
        readonly static string MenuOptions;
        public static ReadOnlyCollection<Project> projects;
        public static ReadOnlyCollection<Task> tasks;
        public static ReadOnlyCollection<Team> teams;
        public static ReadOnlyCollection<User> users;
        public static IEnumerable<DataHierarchy> dataHierarchy;

        static MenuProvider()
        {
            client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            projects = GetProjects();
            tasks = GetTasks();
            teams = GetTeams();
            users = GetUsers();

            dataHierarchy =  from project in projects
                             join task in tasks on project.Id equals task.ProjectId into projectTasks
                             join user in users on project.AuthorId equals user.Id
                             join team in teams on project.TeamId equals team.Id
                             from task in projectTasks
                             join performer in users on task.PerformerId equals performer.Id into taskPerformers
                             select new DataHierarchy { project = project, tasks = projectTasks, performers = taskPerformers, author = user, team = team };

            MenuOptions = "Choose an action: \n" +
                "0. Exit \n" +
                "1. Get user's tasks \n" +
                "2. Get user's finished tasks in 2020 \n" +
                "3. Get team members older 10 \n" +
                "4. Get number of tasks on user's projects \n";

            MenuActions = new Action[] { Exit, GetUserTasks, GetFinishedTasksIn2020, GetTeamMembersOlder10, GetNumberOfTasks, };
        }
        public static void ProvideMenu()
        {
            Console.WriteLine(MenuOptions);
            Console.Write("Your input : ");

            try
            {
                var userInput = Convert.ToInt32(Console.ReadLine());
                MenuActions[userInput]();
                Console.WriteLine();
            }
            catch (FormatException)
            {
                Console.WriteLine("Your input is not a number.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Error. Please enter a number between 0 and {MenuActions.Length}.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception: {exception.Message}");
            }

        }

        private static void Exit()
        {
            toContinue = false;
        }

        private static ReadOnlyCollection<Project> GetProjects()
        {
            HttpResponseMessage response = client.GetAsync("api/projects").Result;

            return response.Content.ReadAsAsync<ReadOnlyCollection<Project>>().Result;

        }

        private static ReadOnlyCollection<Task> GetTasks()
        {
            HttpResponseMessage response = client.GetAsync("api/tasks").Result;

            return response.Content.ReadAsAsync<ReadOnlyCollection<Task>>().Result;

        }

        private static ReadOnlyCollection<Team> GetTeams()
        {
            HttpResponseMessage response = client.GetAsync("api/teams").Result;

            return response.Content.ReadAsAsync<ReadOnlyCollection<Team>>().Result;

        }

        private static ReadOnlyCollection<User> GetUsers()
        {
            HttpResponseMessage response = client.GetAsync("api/Users").Result;

            return response.Content.ReadAsAsync<ReadOnlyCollection<User>>().Result;

        }

        private static void GetUserTasks()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var tasksQuery = from task in tasks
                             where task.PerformerId.Equals(id) && task.Name.Length < 45
                             select task;

                Console.WriteLine("Tasks: ");

                foreach(var item in tasksQuery)
                {
                    Console.WriteLine($"Id: {item.Id} \nName: {item.Name} \nDescription: {item.Description}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static void GetFinishedTasksIn2020()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());

                var tasksQuery = from task in tasks
                             where task.PerformerId.Equals(id) && task.State == 2 && task.FinishedAt.Year == 2020
                             select task;

                Console.WriteLine("Tasks: ");

                foreach (var item in tasksQuery)
                {
                    Console.WriteLine($"Id: {item.Id} \nName: {item.Name} \nDescription: {item.Description}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private static void GetTeamMembersOlder10()
        {
            try
            {
                var teamMembersQuery = users.
                    Where(u => u.Birthday.Year < 2010)
                    .Join(teams, u => u.TeamId, t => t.Id, (u, t) => (team: t, u))
                    .OrderByDescending(x => x.u.RegisteredAt)
                    .GroupBy(x => x.u.TeamId);

                foreach (var g in teamMembersQuery)
                {
                    Console.WriteLine($"Team Id : {g.Key}; Team Name : {g.FirstOrDefault().team.Name}");
                    foreach (var (team, u) in g)
                        Console.WriteLine($"{u.FirstName} {u.LastName} | [{u.RegisteredAt}]");
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void GetNumberOfTasks()
        {
            try
            {
                Console.Write("Enter user ID  : ");
                var id = Convert.ToInt32(Console.ReadLine());
 
                var tasksQuery = projects
                    .Where(item => item.AuthorId == id)
                    .Join(tasks, p => p.Id, t => t.ProjectId, (p, t) => (p, t))
                    .GroupBy(tuple => tuple.p)
                    .ToDictionary(x => x.Key, x => x.Count(tup => tup.p.Id == tup.t.ProjectId));


                Console.WriteLine("Projects: ");

                foreach (var item in tasksQuery)
                {
                    Console.WriteLine($"{item.Key.Name} | Amount of tasks : {item.Value}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
