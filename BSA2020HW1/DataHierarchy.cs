﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA20HW1
{
    public class DataHierarchy
    {
        public Project project;
        public IEnumerable<Task> tasks;
        public IEnumerable<User> performers;
        public User author;
        public Team team;
    }
}
